**Setup and Run Guideline**

---

## Clone sourcecode

git clone git@bitbucket.org:johnathanng/ewrist-ios-test.git

---

## Development Tools:
1. Xcode 11.x
2. Mac OS 10.12 or above

---

## Setup

1. cd to project folder (which contains **Podfile**)
2. pod install
3. Make sure pod install successfully

## Build and Run on device
1. Open Vanilla.xcworkspace by Xcode 11.x
2. Setup account to run on device: Xcode -> Preferences -> Accounts -> Add an Apple Development Account (e.g. montreapp@gmail.com/********)
3. Project properties -> TARGETS -> Vanilla -> Signing: Neeraj Tayal