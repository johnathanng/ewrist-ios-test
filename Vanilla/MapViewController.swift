//
//  ViewController.swift
//  Vanilla
//
//  Created by Tung Pham on 6/3/20.
//  Copyright © 2020 Terralogic. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: UIViewController {
    @IBOutlet weak var vanillaMapView: MKMapView!
    var polyline: MKPolyline?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
    
        setupMapView()
        
        drawLine(VanillaDataManager.shared.getLocationArray())
        
        VanillaDataManager.shared.getPositionsFromDB(at: "[STOP EXERCISE]")
        
        //remove all record in DB when starting new exercise
        VanillaDataManager.shared.removeAllRecord()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.vanillaMapView.delegate = nil
    }
    
    //MARK: MapView Method
    func setupMapView() {
        self.vanillaMapView.showsUserLocation = true
        self.vanillaMapView.showsCompass = true
        self.vanillaMapView.showsScale = true
        self.vanillaMapView.isZoomEnabled = true
        self.vanillaMapView.delegate = self
    }
    
    func clearMap() {
        // remove polyline if one exists
        if let polyline = self.polyline {
            self.vanillaMapView.removeOverlay(polyline)
        }
    }
    
    @IBAction func sendlogButtonPressed(_ sender: Any) {
        LogVC.uploadLogFile(completion: {
            [weak self] (error, res) in
            guard let self = self else {return}

            let alert = UIAlertController(title: nil, message: "Upload succeeded! Log url is copied to clipboard.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

            let pasteBoard = UIPasteboard.general
            pasteBoard.string = res?.url ?? ""
        })
    }
    
    func drawLine(_ coordinateArr: [CLLocationCoordinate2D]) {
        clearMap()
        
        let cordinates = coordinateArr
        
        if let currentLocation = cordinates.first {
            let coordinateRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: 800, longitudinalMeters: 800)
            self.vanillaMapView.setRegion(coordinateRegion, animated: true)
        }
        
        // create a polyline with all cooridnates
        let mkPolyLine: MKPolyline = MKPolyline(coordinates: cordinates, count: cordinates.count)
        self.vanillaMapView.addOverlay(mkPolyLine)
        self.polyline = mkPolyLine
    }
    
    deinit {
        print("Map view controller released")
    }
}

//MARK: - MKMapViewDelegate method
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(polyline: polyline)
            polylineRenderer.strokeColor = UIColor.red
            polylineRenderer.lineWidth = 3
            
            return polylineRenderer
        }
        
        fatalError("Something wrong...")
        //return MKPolylineRenderer()
    }
}

